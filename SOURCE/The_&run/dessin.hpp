#ifndef DESSIN_H_INCLUDE
#define DESSIN_H_INCLUDE
#define h_fen 600
#define l_fen 800
#include <SFML/Graphics.hpp>
using namespace sf;
typedef struct {
    char obs; //Numero / type d'obstacle dans la case
    int joueur; //Booleen de la presence du joueur (1 -> le joueur est sur la case, 0 -> il n'est pas sur la case)
    char item; //Numero / type d'item sur la case
} Case;

void initLigneTerrain(Case *terrain,int nbColonne, int numLigne);
void createTile(int nbColonneTerrain,Sprite table[], Texture table_texture[]);
#endif // DESSIN_H_INCLUDE
