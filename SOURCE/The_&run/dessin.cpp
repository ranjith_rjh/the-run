#include "dessin.hpp"

void initLigneTerrain(Case *terrain,int nbColonne, int numLigne) {
    for(int i = 0; i < nbColonne; i++) {
        terrain[i] = {'C',0,'C'};
    }
}

//LA FONCTION CREER PLUSIEURS TEXTURE ET TILE EN FONCTION DU NOMBRE INDIQUE
void createTile(int nbColonneTerrain,Sprite table[], Texture table_texture[])
{
    for(int i=0;i<nbColonneTerrain;i++)
    {
        Texture t;
        if(!t.loadFromFile("Tile.png"))
            printf("PB DE CHARGEMENT");
        table_texture[i]=t;
        Sprite tile(table_texture[i]);
        int largeur_sprite=tile.getTexture()->getSize().x * tile.getScale().x;
        int hauteur_sprite=tile.getTexture()->getSize().y * tile.getScale().y;
        tile.setPosition(i*largeur_sprite,h_fen-hauteur_sprite);
        table[i]=tile;
    }
}
