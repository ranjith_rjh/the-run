#include "dessin.hpp"



int main()
{
    //Constantes (dans le futur, on pourra changer la taille de l'ecran, donc ce ne seront pas des const)
    const int nbColonneTerrain = 5, nbLigneTerrain = 3; //Nombres de case du terrain
    const int pas = 1;
    //const int h_fen = 600, l_fen = 800; //Largeur et hauteur de la fenetre par defaut
    RenderWindow fen(VideoMode(l_fen, h_fen), "The &run");
    fen.setFramerateLimit(30);

    //Creation du terrain avec la fonction initTerrain
    Case terrain[nbLigneTerrain][nbColonneTerrain];
    for(int i = 0; i < nbLigneTerrain; i++) {
        initLigneTerrain(terrain[i], nbColonneTerrain, i);
    }

    Sprite table_sprite[nbColonneTerrain];
    Texture table_texture[nbColonneTerrain];
    createTile(nbColonneTerrain,table_sprite,table_texture);

    while(fen.isOpen()) {
        Event event;
        fen.clear();
        while(fen.pollEvent(event)) {
            if(event.type ==Event::Closed) fen.close();
        }

        for(int i=0; i < nbColonneTerrain; i++)
        {
            table_sprite[i].setPosition(table_sprite[i].getPosition().x-pas, table_sprite[i].getPosition().y);
            fen.draw(table_sprite[i]);
        }

        fen.display();
    }
    return 0;
}
